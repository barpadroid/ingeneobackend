﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ingeneo.Domain.Interfaces
{
    public interface ILoadDataRepository
    {
       public Task<Boolean> LoadDataAuthors();
       public Task<Boolean> LoadDataBooks();

    }
}
