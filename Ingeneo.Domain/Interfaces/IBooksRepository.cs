﻿using Ingeneo.Domain.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ingeneo.Domain.Interfaces
{
    public interface IBooksRepository
    {
        //IEnumerable<Books> Get();
        public IEnumerable<Books> GetBooksByFilters(FiltersModel filters);
    }
}
