﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ingeneo.Domain.Services
{
    public interface ILoadDataServices
    {
       public Task<Boolean> LoadDataAuthors();
       public Task<Boolean> LoadDataBooks();
    }
}
