﻿using Ingeneo.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ingeneo.Domain.Services
{
    public class AuthorsServices : IAuthorsServices
    {
        IAuthorsRepository AuthorsRepository;

        public AuthorsServices(IAuthorsRepository authorsRepository)
        {
            AuthorsRepository = authorsRepository;
        }
    }
}
