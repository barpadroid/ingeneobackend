﻿using Ingeneo.Domain.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ingeneo.Domain.Services
{
    public interface IBooksServices
    {
        public IEnumerable<Books> GetBooksByFilters(FiltersModel filters);
    }
}
