﻿using Ingeneo.Domain.DomainModels;
using Ingeneo.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ingeneo.Domain.Services
{
    public class BooksServices : IBooksServices
    {
        IBooksRepository BooksRepository;

        public BooksServices(IBooksRepository booksRepository)
        {
            BooksRepository = booksRepository;
        }

        public IEnumerable<Books> GetBooksByFilters(FiltersModel filters)
        {
            return BooksRepository.GetBooksByFilters(filters);
        }
    }
}
