﻿using Ingeneo.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ingeneo.Domain.Services
{
    public class LoadDataServices : ILoadDataServices
    {
        ILoadDataRepository LoadDataRepository;

        public LoadDataServices(ILoadDataRepository loadDataRepository)
        {
            LoadDataRepository = loadDataRepository;
        }

        public Task<Boolean> LoadDataAuthors()
        {
            return LoadDataRepository.LoadDataAuthors();
        }

        public Task<Boolean> LoadDataBooks()
        {
            return LoadDataRepository.LoadDataBooks();
        }
    }
}
