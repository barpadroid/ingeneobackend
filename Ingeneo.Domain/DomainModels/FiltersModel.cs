﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ingeneo.Domain.DomainModels
{
    public class FiltersModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
