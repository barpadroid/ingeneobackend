﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Ingeneo.Infrastructure
{
    public class InfrastructureStartup
    {
        public static void ConfigureServices(IServiceCollection services, string connectionString)
        {
            services.AddDbContext<Models.AppDbContext>(options => options.UseSqlServer(connectionString));
        }
    }
}
