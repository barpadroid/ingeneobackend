﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ingeneo.Infrastructure.Models
{
	public class Authors
	{
		[Key]
		public int Id { get; set; }
		public int IdBook { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
