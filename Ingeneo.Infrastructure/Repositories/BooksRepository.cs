﻿using Ingeneo.Domain.DomainModels;
using Ingeneo.Domain.Interfaces;
using Ingeneo.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ingeneo.Infrastructure.Repositories
{
    public class BooksRepository : IBooksRepository
	{
		private readonly AppDbContext context;
		public BooksRepository(AppDbContext context)
		{
			this.context = context;
		}
		public IEnumerable<Domain.DomainModels.Books> GetBooksByFilters(FiltersModel filters)
		{
			var resultado = context.Books;
			var authorRersult = context.Authors;

			var idBookOfAuthors = (from a in authorRersult
								   where a.FirstName.Equals(filters.FirstName) || a.LastName.Equals(filters.LastName)
								   select a.IdBook);

            if (idBookOfAuthors.Count() > 0)
            {
				return resultado.Select(books => new Domain.DomainModels.Books
				{
					Id = books.Id,
					Title = books.Title,
					Description = books.Description,
					PageCount = books.PageCount,
					Excerpt = books.Excerpt,
					PublishDate = books.PublishDate,
				}).Where(book => book.PublishDate >= filters.StartDate && book.PublishDate <= filters.EndDate && book.Id.Equals(idBookOfAuthors.First())).OrderBy(t => t.PublishDate).ToList();

            }
            else
            {
				return resultado.Select(books => new Domain.DomainModels.Books
				{
					Id = books.Id,
					Title = books.Title,
					Description = books.Description,
					PageCount = books.PageCount,
					Excerpt = books.Excerpt,
					PublishDate = books.PublishDate,
				}).Where(book => book.PublishDate >= filters.StartDate && book.PublishDate <= filters.EndDate).OrderBy(t => t.PublishDate).ToList();

			}
		}

	}
}
