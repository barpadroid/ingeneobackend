﻿using Ingeneo.Domain.Interfaces;
using Ingeneo.Infrastructure.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Ingeneo.Infrastructure.Repositories
{
    public class LoadDataRepository : ILoadDataRepository
	{
		private readonly AppDbContext context;
		public LoadDataRepository(AppDbContext context)
		{
			this.context = context;
		}

		public async Task<Boolean> LoadDataAuthors()
		{
			var httpClient = new HttpClient();
			var auhtorsJson = await httpClient.GetStringAsync("https://fakerestapi.azurewebsites.net/api/v1/Authors");
			var authorsList = JsonConvert.DeserializeObject<List<Authors>>(auhtorsJson);
			context.Authors.RemoveRange(context.Authors);
			context.Authors.AddRange(authorsList);
			context.SaveChanges();
			return true;
		}

		public async Task<Boolean> LoadDataBooks()
		{
			var httpClient = new HttpClient();
			var booksJson = await httpClient.GetStringAsync("https://fakerestapi.azurewebsites.net/api/v1/Books");
			var boksList = JsonConvert.DeserializeObject<List<Books>>(booksJson);
			context.Books.RemoveRange(context.Books);
			context.Books.AddRange(boksList);
			context.SaveChanges();
			return true;
		}
	}
}
