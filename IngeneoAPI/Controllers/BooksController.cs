﻿using Ingeneo.Domain.DomainModels;
using Ingeneo.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IngeneoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly ILogger<AuthorsController> _logger;
        private readonly IBooksRepository _booksRepository;
        public BooksController(ILogger<AuthorsController> logger, IBooksRepository booksRepository)
        {
            _logger = logger;
            _booksRepository = booksRepository ?? throw new ArgumentException(nameof(booksRepository));
        }
        [Route("GetBooksByFilters")]
        [HttpPost]
        public IEnumerable<Books> GetBooksByFilters(FiltersModel request)
        {
            return _booksRepository.GetBooksByFilters(request);
        }
    }
}
