﻿using Ingeneo.Domain.Interfaces;
using IngeneoAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AuthorsAndBooksTest.Controllers
{
	/// <summary>
	/// Authors Controller
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	public class LoadDataController : Controller
	{
		private readonly ILogger<AuthorsController> _logger;
		private readonly ILoadDataRepository _loadDataRepository;
		public LoadDataController(ILogger<AuthorsController> logger, ILoadDataRepository loadDataRepository)
		{
			_logger = logger;
			_loadDataRepository = loadDataRepository ?? throw new ArgumentException(nameof(loadDataRepository));
		}

		[Route("LoadDataAuthors")]
		[HttpGet]
		public async Task<IActionResult> LoadDataAuthors()
		{
			var synchronization = await _loadDataRepository.LoadDataAuthors();
			return Ok(synchronization);
		}

		[Route("LoadDataBooks")]
		[HttpGet]
		public async Task<IActionResult> LoadDataBooks()
		{
			return Ok(await _loadDataRepository.LoadDataBooks());
		}
	}
}
