using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.OpenApi.Models;
using Ingeneo.Domain.Interfaces;
using Ingeneo.Domain.Services;
using Ingeneo.Infrastructure.Repositories;
using Ingeneo.Infrastructure;

namespace IngeneoAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var configurationString = Configuration["StoreDBContext"];
            InfrastructureStartup.ConfigureServices(services, configurationString);
            services.AddScoped<IAuthorsServices, AuthorsServices>();
            services.AddScoped<IAuthorsRepository, AuthorsRepository>();
            services.AddScoped<IBooksServices, BooksServices>();
            services.AddScoped<IBooksRepository, BooksRepository>();
            services.AddScoped<ILoadDataServices, LoadDataServices>();
            services.AddScoped<ILoadDataRepository, LoadDataRepository>();
            services.AddControllersWithViews();
            //Enable CORS
            services.AddCors(c => {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
            services.AddControllers();
            AddSwagger(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ingeneo API v1");
            });
            // Enable Cors
            app.UseCors("AllowOrigin");
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"API services {groupName}",
                    Version = groupName,
                    Description = "Ingeneo API",
                    Contact = new OpenApiContact
                    {
                        Name = "Ingeneo",
                        Email = string.Empty,
                        Url = new Uri("https://ingeneo.com.co/"),
                    }
                });
            });
        }
    }
}
